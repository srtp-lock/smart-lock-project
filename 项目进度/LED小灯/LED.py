# encoding: utf-8
import RPI.GPIO as GPIO
import time

# 指定GPIO口得选定模式为GPIO引脚编号模式（非主板编号模式）
GPIO.setmode(GPIO.BCM)


# 指定GPIO26的模式为输出模式

while True:
    GPIO.output(26,True)
    time.sleep(0.5)
    GPIO.output(26,False)
    time.sleep(0.5)
    