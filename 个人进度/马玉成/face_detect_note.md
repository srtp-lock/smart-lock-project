```python
from matplotlib import pyplot as plt
import numpy as np
import cv2 as cv
```


```python
img = cv.imread("Lenna.jpg")
cv.imshow('Lenna', img)
plt.imshow(img[:,:,::-1])
cv.destroyAllWindows()
```


    
![png](face_detect_note_files/face_detect_note_1_0.png)
    



```python
def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30),
                                     flags=cv.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects
```


```python
def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv.rectangle(img, (x1, y1), (x2, y2), color, 2)
```


```python
cv.samples.addSamplesDataSearchPath("/usr/share/opencv4")
path = cv.samples.findFile("haarcascades/haarcascade_frontalface_default.xml")
cascade = cv.CascadeClassifier(path)
rects = detect(img, cascade)
draw_rects(img, rects, (255, 255, 0))
```

    [ WARN:0@2195.838] global /io/opencv/modules/core/src/utils/samples.cpp (61) findFile cv::samples::findFile('haarcascades/haarcascade_frontalface_default.xml') => '/usr/share/opencv4/haarcascades/haarcascade_frontalface_default.xml'



```python
plt.imshow(img[:,:,::-1])
cv.destroyAllWindows()
```


    
![png](face_detect_note_files/face_detect_note_5_0.png)
    

